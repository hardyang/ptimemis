<?php return array (
  'base' => 
  array (
    'name' => 'category',
    'comment' => '分类表',
  ),
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => 'PRI',
      'default' => NULL,
      'comment' => '',
      'is_hide' => 0,
    ),
    1 => 
    array (
      'name' => 'name',
      'type' => 'varchar(20)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '名称',
      'is_hide' => 0,
    ),
    2 => 
    array (
      'name' => 'en_name',
      'type' => 'varchar(40)',
      'null' => 'YES',
      'key' => '',
      'default' => NULL,
      'comment' => '英文名称',
      'is_hide' => 0,
    ),
    3 => 
    array (
      'name' => 'pic_dir',
      'type' => 'varchar(200)',
      'null' => 'YES',
      'key' => '',
      'default' => NULL,
      'comment' => '关联图片地址',
      'is_hide' => 0,
    ),
    4 => 
    array (
      'name' => 'father_id',
      'type' => 'int(11)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '父级分类id',
      'is_hide' => 0,
      'relate'=> [
        'table' =>'category',
        'field' =>'id',
        'select'=>'name',
        'filter' =>"",
        'name'  =>'category_name',
        'comment' => '父级分类'
      ]
    ),
    5 => 
    array (
      'name' => 'father_path',
      'type' => 'varchar(500)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '父级分类路径',
      'is_hide' => 0,
    ),
    6 => 
    array (
      'name' => 'module',
      'type' => 'varchar(20)',
      'null' => 'NO',
      'key' => '',
      'default' => NULL,
      'comment' => '应用模型',
      'is_hide' => 0,
    ),
    7 => 
    array (
      'name' => 'weight',
      'type' => 'smallint(6)',
      'null' => 'YES',
      'key' => '',
      'default' => '0',
      'comment' => '排序权重',
      'is_hide' => 0,
    ),
    8 => 
    array (
      'name' => 'is_visible',
      'type' => 'tinyint(1)',
      'null' => 'YES',
      'key' => '',
      'default' => '1',
      'comment' => '是否可见',
      'is_hide' => 0,
    ),
    9 => 
    array (
      'name' => 'is_delete',
      'type' => 'tinyint(1)',
      'null' => 'YES',
      'key' => '',
      'default' => '0',
      'comment' => '是否删除',
      'is_hide' => 0,
    ),
  ),
);